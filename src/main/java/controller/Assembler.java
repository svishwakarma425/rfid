package controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import config.AppConfig;
import mongo.CheckEvent;
import mongo.SendToMongo;
import utils.*;
import webservice.LocationUpdate;
import webservice.RestApiInvoker;

import java.io.IOException;
import java.util.Map;

/**
 * Created by satya on 11/1/18.
 */
public class Assembler {

    public static void doTheNeedFull(String value){
        ObjectMapper mapper = new ObjectMapper();
        JsonTransmitProtocolSchema jsonReqObj=new JsonTransmitProtocolSchema();
        try {
            jsonReqObj = mapper.readValue(value, JsonTransmitProtocolSchema.class);
            System.out.println("jsonReqObj="+jsonReqObj);
            if(jsonReqObj.info.txn.equals("W")){
                System.out.println("RFID event occurs :"+jsonReqObj.info.txn);
                //Save Event
                SendToMongo event_obj=new SendToMongo();
                System.out.println("Save Event : "+event_obj.save(value));
                //Check event for
                if(jsonReqObj.info.cmdval!=null && !jsonReqObj.info.cmdval.equals("")){
                    CheckEvent chcekObj=new CheckEvent();
                    String event_for=chcekObj.getEventFor(jsonReqObj.info.cmdval);

                    if(event_for.equals(AppConfig.DRIVER)){
                        //Driver Operation....
                    }else if(event_for.equals(AppConfig.STUDENT)){
                        //Student operation....
                        StudentAction stdObj=new StudentAction();
                        stdObj.invoke(chcekObj,jsonReqObj);
                    }
                }
            }else {
              System.out.println("RFID event txn is :"+jsonReqObj.info.txn);
              //call location update api.
                // http://4alzlx.hexarides.com/T2kKcZenR7
                LocationUpdate.locationUpdate(jsonReqObj);
            }
        } catch (JsonProcessingException e) {
            System.out.println(" JsonProcessingException="+e.toString());

        } catch (IOException e) {
            System.out.println("IOException "+e.toString());

        }
    }
}
