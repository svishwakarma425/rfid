package controller;

import config.AppConfig;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.kstream.ForeachAction;
import org.apache.kafka.streams.kstream.KStream;
import org.apache.kafka.streams.kstream.KStreamBuilder;
import org.apache.kafka.streams.processor.WallclockTimestampExtractor;
import java.util.Properties;

/**
 * Created by satya on 3/1/18.
 */


public class Catcher {

    //static Logger slf4jLogger = null;
    public static void main(final String[] args) throws Exception {
        //slf4jLogger = LoggerFactory.getLogger(KafkaReader.class);
        try {
            String topics= AppConfig.TOPIC_NAME;
            String app_id_config="rfid_app_id_100";
            String kafka_c1="159.89.164.241:9092";
            String kafka_c2="159.89.160.45:9092";
            String kafka_c3="159.89.164.82:9092";
            String broker_comma_separated=kafka_c1+","+kafka_c2+","+kafka_c3;

            final Properties props = new Properties();
            props.put(StreamsConfig.APPLICATION_ID_CONFIG, app_id_config);
            //props.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
            props.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, broker_comma_separated);
            props.put(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.String().getClass().getName());
            props.put(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, Serdes.String().getClass().getName());
            props.put(StreamsConfig.COMMIT_INTERVAL_MS_CONFIG, 10 * 1000);
            props.put(StreamsConfig.DEFAULT_TIMESTAMP_EXTRACTOR_CLASS_CONFIG, WallclockTimestampExtractor.class);

            final Serde<String> stringSerde = Serdes.String();
            final Serde<Long> longSerde = Serdes.Long();
            final KStreamBuilder builder = new KStreamBuilder();

            final KStream<String, String> textLines = builder.stream(stringSerde, stringSerde, topics);
            System.out.println("We are testing here topics: "+topics+" broker_comma_separated: "+broker_comma_separated);
            //slf4jLogger.debug("We are testing here=====");

            textLines.foreach(new ForeachAction<String, String>() {
                public void apply(String key, String value) {
                    //String userName=key.replace("\"", "");
                    //System.out.println("key==="+key);
                    System.out.println("*********Start******");
                    System.out.println("value="+value);
                    Assembler.doTheNeedFull(value);
                    System.out.println("********End*********");

                }
            });


            final KafkaStreams streams = new KafkaStreams(builder, props);
            streams.cleanUp();
            streams.start();
            Runtime.getRuntime().addShutdownHook(new Thread(streams::close));
        }catch(Exception e) {
            System.out.println("error=================="+e.getMessage());
            //slf4jLogger.debug("error================slf4jLogger"+e.getMessage());
        }

    }
}

