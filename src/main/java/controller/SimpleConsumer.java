package controller;

import java.util.Properties;
import java.util.Arrays;

import config.AppConfig;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.common.TopicPartition;

/**
 * Created by satya on 9/1/18.
 */
public class SimpleConsumer {

    public static void main(String[] args) throws Exception {

        //Kafka consumer configuration settings
        //String topicName= "SatyaTopic3";
        String topicName= AppConfig.TOPIC_NAME;
        String app_id_config="rfid_app_id_100";
        String group_id="KafkaConsumerForRFID";
        String kafka_c1="159.89.164.241:9092";
        String kafka_c2="159.89.160.45:9092";
        String kafka_c3="159.89.164.82:9092";
        String broker_comma_separated=kafka_c1+","+kafka_c2+","+kafka_c3;
        Properties props = new Properties();

        props.put("bootstrap.servers", broker_comma_separated);
        props.put("group.id", group_id);
        props.put("enable.auto.commit", "true");
        props.put("auto.commit.interval.ms", "1000");
        props.put("session.timeout.ms", "30000");
        props.put("key.deserializer",
                "org.apache.kafka.common.serialization.StringDeserializer");
        props.put("value.deserializer",
                "org.apache.kafka.common.serialization.StringDeserializer");
        KafkaConsumer<String, String> consumer = new KafkaConsumer
                <String, String>(props);


        //TopicPartition partition0 = new TopicPartition(topicName, 0);
        //TopicPartition partition1 = new TopicPartition(topicName, 1);
        //consumer.assign(Arrays.asList(partition0,partition1));
        //Kafka Consumer subscribes list of topics here.
        consumer.subscribe(Arrays.asList(topicName));



        //print the topic name
        System.out.println("Subscribed to topic " + topicName);
        int i = 0;

        while (true) {
            ConsumerRecords<String, String> records = consumer.poll(100);
            for (ConsumerRecord<String, String> record : records){

                // print the offset,key and value for the consumer records.
                System.out.printf("topic = %s,partition = %d,offset = %d, key = %s, value = %s\n",
                        record.topic(),record.partition(),record.offset(), record.key(), record.value());
                Assembler.doTheNeedFull(record.value());
            }


        }
    }
}
