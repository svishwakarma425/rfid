package controller;

import config.AppConfig;
import mongo.CheckEvent;
import utils.Decision;
import utils.Driver;
import utils.JsonTransmitProtocolSchema;
import webservice.RestApiInvoker;

import java.util.Map;

/**
 * Created by satya on 1/2/18.
 */
public class StudentAction {

    public void invoke(CheckEvent chcekObj, JsonTransmitProtocolSchema jsonReqObj){

        //Student operation....
        Driver driverObj=chcekObj.getDriver(jsonReqObj.info.cmdval);//ReadCSV.getDriver(String.valueOf(jsonReqObj.uid));
        if(!driverObj.email.equals("") && driverObj.email!=null){
            DBOperation dbObj=new DBOperation();
            Map<String,String> stdStateMap=dbObj.getStudent(jsonReqObj);
            if(stdStateMap!=null && stdStateMap.size()>0){
                //Map<String, String> stdLocMap=//dbObj.getRange(jsonReqObj,stdStateMap);
                String decision= Decision.decideStudentLocation(stdStateMap);
                if(decision!=null){
                    RestApiInvoker obj=new RestApiInvoker();
                    if(decision.equals(AppConfig.PICKUP_FROM_HOME)){
                        obj.pickUpFromHome(stdStateMap.get("rider_id"),
                                AppConfig.PICKUP_FROM_HOME,
                                String.valueOf(jsonReqObj.gps.loc[0]),
                                String.valueOf(jsonReqObj.gps.loc[1]),
                                driverObj.user_id,
                                driverObj.email
                        );
                    }else if(decision.equals(AppConfig.DROP_AT_SCHOOL)){
                        obj.pickUpFromHome(stdStateMap.get("rider_id"),
                                AppConfig.DROP_AT_SCHOOL,
                                String.valueOf(jsonReqObj.gps.loc[0]),
                                String.valueOf(jsonReqObj.gps.loc[1]),
                                driverObj.user_id,
                                driverObj.email
                        );

                    }else if(decision.equals(AppConfig.PICKUP_FROM_SCHOOL)){
                        obj.pickUpFromHome(stdStateMap.get("rider_id"),
                                AppConfig.PICKUP_FROM_SCHOOL,
                                String.valueOf(jsonReqObj.gps.loc[0]),
                                String.valueOf(jsonReqObj.gps.loc[1]),
                                driverObj.user_id,
                                driverObj.email
                        );

                    }else if(decision.equals(AppConfig.DROP_AT_HOME)){
                        obj.pickUpFromHome(stdStateMap.get("rider_id"),
                                AppConfig.DROP_AT_HOME,
                                String.valueOf(jsonReqObj.gps.loc[0]),
                                String.valueOf(jsonReqObj.gps.loc[1]),
                                driverObj.user_id,
                                driverObj.email
                        );

                    }
                }
            }else {
                System.out.println("No Student found for :"+jsonReqObj.info.cmdval);
            }
        }

    }
}
