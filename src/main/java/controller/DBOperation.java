package controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import pgdb.DB_Connection;
import utils.*;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by satya on 10/1/18.
 */
public class DBOperation {

    public Map<String,String> getRange(JsonTransmitProtocolSchema request, Map<String,String> stdMap){
        Map<String, String> stdLocMap = new HashMap<String, String>();
        if(stdMap!=null && stdMap.size()>0){
            String student_location="select " +
                    " ST_Distance_Sphere(rider.curr_loc,ST_SetSRID(ST_MakePoint('"+request.gps.loc[0]+"','"+request.gps.loc[1]+"'), 4326)) as distance_from_home ," +
                    " ST_Distance_Sphere(school.curr_loc,ST_SetSRID(ST_MakePoint('"+request.gps.loc[0]+"','"+request.gps.loc[1]+"'), 4326)) as distance_from_school " +
                    " from api_student as student" +
                    " INNER JOIN api_rider  as rider ON (student.rider_id = rider.id)" +
                    " INNER JOIN api_school  as school ON (student.school_id = school.id)" +
                    " where rider.id="+stdMap.get("rider_id");
            System.out.println("student_location="+student_location);
            Connection con = null;
            Statement stmt = null;
            ResultSet rs =null;
            StudentLocation studentObj=new StudentLocation();

            try{
                con= DB_Connection.getDbConnection();
                stmt = con.createStatement();
                rs = stmt.executeQuery(student_location);
                System.out.println("Result set in range :"+rs.getFetchSize());
                while ( rs.next() ) {
                    stdLocMap.put("distance_from_home",String.valueOf(rs.getLong("distance_from_home")));
                    stdLocMap.put("distance_from_school",String.valueOf(rs.getLong("distance_from_school")));
                    System.out.println("range in meter in aerial distance=====>"+studentObj);
                }
                rs.close();
                stmt.close();
                con.close();
            }catch(SQLException e){
                System.out.println("Exception at: getStudentLocation "+e.getMessage());
            }
            catch(Exception e){
                System.out.println("Exception at: getStudentLocation "+e.getMessage());
            }
    }
        return stdLocMap;
    }

    public Map<String,String> getStudent(JsonTransmitProtocolSchema request){

        String getStudentQuery="select student.rider_id as rider_id ,rider.avail as avail " +
                " from api_student as student" +
                " INNER JOIN api_rider  as rider ON (student.rider_id = rider.id)" +
                " where  student.ref_rfid = '"+request.info.cmdval+"'";

        System.out.println("getStudentQuery= "+getStudentQuery);
        //Connection con = null;
        //Statement stmt = null;
        //ResultSet rs =null;
        Map<String, String> studentMap = new HashMap<String, String>();
        try{
            Connection con= DB_Connection.getDbConnection();
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery(getStudentQuery);
            System.out.println("Result set getStudent="+rs.getFetchSize());
            while ( rs.next() ){
                studentMap.put("rider_id",String.valueOf(rs.getInt("rider_id")));
                studentMap.put("avail",String.valueOf(rs.getInt("avail")));
                System.out.println("studentMap in api_rider=====>"+studentMap);
            }

            rs.close();
            stmt.close();
            con.close();
        }catch(SQLException e){
            System.out.println("Exception at: getStudentLocation "+e.getMessage());
        }catch(Exception e){
            System.out.println("Exception at: getStudentLocation "+e.getMessage());
        }
        return studentMap;

    }


    public static void main(String arg[]){
        DBOperation obj=new DBOperation();
        //obj.getRange();

    }
}
