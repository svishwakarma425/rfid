package webservice;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import config.AppConfig;
import utils.JsonTransmitProtocolSchema;
import utils.ReadCSV;

/**
 * Created by satya on 16/1/18.
 */
public class LocationUpdate {
    public static void locationUpdate(JsonTransmitProtocolSchema jsonReqObj){

        try{
            String driverEmail= ReadCSV.getDriver(jsonReqObj.uid).email;
            String body="{\"records\":[{\"key\":\""+driverEmail+"\", \"value\":{\"loc_lat\":\""+jsonReqObj.gps.loc[0]+"\",\"loc_lng\":\""+jsonReqObj.gps.loc[1]+"\",\"alt\":\""+jsonReqObj.gps.alt+"\",\"bearing\":\""+jsonReqObj.gps.dir+"\",\"accuracy\":\"20.0\",\"time_stamp\":\""+System.currentTimeMillis()+"\",\"sequence\":\""+jsonReqObj.info.msgid+"\",\"curr_speed\":\""+jsonReqObj.gps.speed+"\"}}]}";
            System.out.println("Location insert Request Body: "+body);
            HttpResponse<String> response = Unirest.post(AppConfig.LOCATION_UPDATE_URL)
                    .header("content-type", "application/vnd.kafka.json.v1+json")
                    .body(body)
                    .asString();
            System.out.println("Location Response status: "+response.getStatus()+" Body: "+response.getBody());
        }catch(UnirestException e){
            System.err.println("===="+e.getMessage());
        }
    }
}
