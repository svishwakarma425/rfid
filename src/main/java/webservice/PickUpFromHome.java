package webservice;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import config.AppConfig;
import controller.DBOperation;
import utils.JsonTransmitProtocolSchema;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import static utils.ReadCSV.getDriver;

/**
 * Created by satya on 11/1/18.
 */
public class PickUpFromHome {




    public void callUnirestPost(){
        try{
            HttpResponse<String> response = Unirest.post("http://localhost:9000/token")
                    .header("content-type", "application/x-www-form-urlencoded")
                    .header("cache-control", "no-cache")
                    .header("Set-Cookie","PLAY_SESSION=7463ccc3a5b845d8ef85e10cca770a1aa296dd82-uIlAqmu7nh37BXfyiClSd9H8D3AD5u=satyadriver%40hexa.com")

                    .body("username=satyadriver@hexa.com&password=get2work&grant_type=password&client_id=Sfm4J9upyYZ0pW96CnTt3OsS9LlgsE0aLFQTLIWT&client_secret=mubi66tclHG8GWVJMC5AIDKGBnvJotdZgfjdknh2DSbw6UfEmD4rq7gH3Z9M60CVDfStvRtDHxmEaH1LI2XKxU42AXofiV26T7XKRgV0rmcJyPUKMHfyjSscFUX1n9la")
                    .asString();
            System.out.println("response===="+response.getBody());
        }catch(UnirestException e){
            System.out.println("===="+e.getMessage());
        }

    }
    public void callUnirestPost1(){
        try{
            HttpResponse<String> response = Unirest.post("http://139.59.24.227:9000/pickup/home")
                    .header("content-type", "application/x-www-form-urlencoded")
                    .header("cache-control", "no-cache")
                    .body("rider_id=2&avail=1&loc_lat=22.574761&loc_lng=88.433778&uid=1739010014027&driver_id=401&driver_email=satyadriver@hexa.com")
                    .asString();
            System.out.println("response===="+response.getBody());
        }catch(UnirestException e){
            System.out.println("===="+e.getMessage());
        }

    }

    public void locationUpdate(JsonTransmitProtocolSchema jsonReqObj,String driverEmail){

        try{
            String body="{\"records\":[{\"key\":\""+driverEmail+"\", \"value\":{\"loc_lat\":\""+jsonReqObj.gps.loc[0]+"\",\"loc_lng\":\""+jsonReqObj.gps.loc[1]+"\",\"alt\":\""+jsonReqObj.gps.alt+"\",\"bearing\":\""+jsonReqObj.gps.dir+"\",\"accuracy\":\"20.0\",\"time_stamp\":\""+System.currentTimeMillis()+"\",\"sequence\":\""+jsonReqObj.info.msgid+"\",\"curr_speed\":\""+jsonReqObj.gps.speed+"\"}}]}";
            System.out.println("Request Body: "+body);
            HttpResponse<String> response = Unirest.post(AppConfig.LOCATION_UPDATE_URL)
                    .header("content-type", "application/vnd.kafka.json.v1+json")
                    .body(body)
                    .asString();
            System.out.println("Response status: "+response.getStatus()+" Body: "+response.getBody());
        }catch(UnirestException e){
            System.out.println("===="+e.getMessage());
        }
    }

    public static void main(final String[] args){

    }
}

