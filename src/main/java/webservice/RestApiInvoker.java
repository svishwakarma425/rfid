package webservice;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import config.AppConfig;

/**
 * Created by satya on 12/1/18.
 */
public class RestApiInvoker {

    public void pickUpFromHome(String rider_id,String avail,String loc_lat,String loc_lng,String driver_id,String driver_email){
        System.out.println("pickUpFromHome invokes..");
        try{
            String url_for_pickup_from_home="http://139.59.24.227:9000/pickup/home";
            String body_for_pickup_from_home="rider_id="+rider_id+
                        "&avail="+avail+
                        "&loc_lat="+loc_lat+
                        "&loc_lng="+loc_lng+
                        "&driver_id="+driver_id+
                        "&driver_email="+driver_email;
            System.out.println("pickUpFromHome body="+body_for_pickup_from_home);

            HttpResponse<String> response = Unirest.post(url_for_pickup_from_home)
                    .header("content-type", "application/x-www-form-urlencoded")
                    .header("cache-control", "no-cache")
                    //.body("rider_id=2&avail=1&loc_lat=22.574761&loc_lng=88.433778&uid=1739010014027&driver_id=401&driver_email=satyadriver@hexa.com")
                    .body(body_for_pickup_from_home)
                    .asString();
            System.out.println("pickUpFromHome response Status"+response.getStatus());
            System.out.println("pickUpFromHome response Body"+response.getBody());
            RestApiInvoker notifObj=new RestApiInvoker();
            if(response.getStatus()==200){
                System.out.println("pickUpFromHome success.");
                notifObj.shortMessageServiceToParentPickUp("[{\"id\":\""+rider_id+"\"}]",loc_lat,loc_lng,AppConfig.PICKUP_FROM_HOME);
                notifObj.pushMessageToParentPickUp("[{\"id\":\""+rider_id+"\"}]",loc_lat,loc_lng,AppConfig.PICKUP_FROM_HOME);
            }else{
                System.out.println("pickUpFromHome fail.");
            }
        }catch(UnirestException e){
            System.out.println("===="+e.getMessage());
        }
    }

    public void dropAtSchool(String rider_id,String avail,String loc_lat,String loc_lng,String driver_id,String driver_email){
        System.out.println("dropAtSchool invokes..");
        try{
            String url_for_dropAtSchool="http://139.59.24.227:9000/drop/school";
            String body_for_dropAtSchool="rider_id="+rider_id+
                    "&avail="+avail+
                    "&loc_lat="+loc_lat+
                    "&loc_lng="+loc_lng+
                    "&driver_id="+driver_id+
                    "&driver_email="+driver_email;
            System.out.println("pickUpFromHome body="+body_for_dropAtSchool);

            HttpResponse<String> response = Unirest.post(url_for_dropAtSchool)
                    .header("content-type", "application/x-www-form-urlencoded")
                    .header("cache-control", "no-cache")
                    .body(body_for_dropAtSchool)
                    .asString();
            System.out.println("dropAtSchool response Status: "+response.getStatus());
            System.out.println("dropAtSchool response Body: "+response.getBody());
            RestApiInvoker notifObj=new RestApiInvoker();
            if(response.getStatus()==200){
                System.out.println("dropAtSchool success.");
                notifObj.shortMessageServiceToParentDrop("[{\"id\":\""+rider_id+"\"}]",loc_lat,loc_lng, AppConfig.DROP_AT_SCHOOL);
                notifObj.pushMessageToParentDrop("[{\"id\":\""+rider_id+"\"}]",loc_lat,loc_lng,AppConfig.DROP_AT_SCHOOL);
            }else{
                System.out.println("dropAtSchool fail.");
            }
        }catch(UnirestException e){
            System.out.println("===="+e.getMessage());
        }
    }
    public void pickUpFromSchool(String rider_id,String avail,String loc_lat,String loc_lng,String driver_id,String driver_email){
        System.out.println("pickUpFromSchool invokes..");
        try{
            String url_for_pickUpFromSchool="http://139.59.24.227:9000/pickup/school";
            String body_for_pickUpFromSchool="rider_id="+rider_id+
                    "&avail="+avail+
                    "&loc_lat="+loc_lat+
                    "&loc_lng="+loc_lng+
                    "&driver_id="+driver_id+
                    "&driver_email="+driver_email;
            System.out.println("pickUpFromSchool body="+body_for_pickUpFromSchool);

            HttpResponse<String> response = Unirest.post(url_for_pickUpFromSchool)
                    .header("content-type", "application/x-www-form-urlencoded")
                    .header("cache-control", "no-cache")
                    .body(body_for_pickUpFromSchool)
                    .asString();
            System.out.println("pickUpFromHome response Status"+response.getStatus());
            System.out.println("pickUpFromHome response Body"+response.getBody());
            RestApiInvoker notifObj=new RestApiInvoker();
            if(response.getStatus()==200){
                System.out.println("pickUpFromSchool success.");
                notifObj.shortMessageServiceToParentPickUp("[{\"id\":\""+rider_id+"\"}]",loc_lat,loc_lng,AppConfig.PICKUP_FROM_SCHOOL);
                notifObj.pushMessageToParentPickUp("[{\"id\":\""+rider_id+"\"}]",loc_lat,loc_lng,AppConfig.PICKUP_FROM_SCHOOL);
            }else{
                System.out.println("pickUpFromSchool fail.");
            }
        }catch(UnirestException e){
            System.out.println("===="+e.getMessage());
        }
    }
    public void dropAtHome(String rider_id,String avail,String loc_lat,String loc_lng,String driver_id,String driver_email){
        System.out.println("dropAtHome invokes..");
        try{
            String url_for_dropAtHome="http://139.59.24.227:9000/drop/school";
            String body_for_dropAtHome="rider_id="+rider_id+
                    "&avail="+avail+
                    "&loc_lat="+loc_lat+
                    "&loc_lng="+loc_lng+
                    "&driver_id="+driver_id+
                    "&driver_email="+driver_email;
            System.out.println("url_for_dropAtHome body="+url_for_dropAtHome);

            HttpResponse<String> response = Unirest.post(url_for_dropAtHome)
                    .header("content-type", "application/x-www-form-urlencoded")
                    .header("cache-control", "no-cache")
                    .body(body_for_dropAtHome)
                    .asString();
            System.out.println("dropAtHome response Status: "+response.getStatus());
            System.out.println("dropAtHome response Body: "+response.getBody());
            RestApiInvoker notifObj=new RestApiInvoker();
            if(response.getStatus()==200){
                System.out.println("dropAtHome success.");
                notifObj.shortMessageServiceToParentDrop("[{\"id\":\""+rider_id+"\"}]",loc_lat,loc_lng, AppConfig.DROP_AT_HOME);
                notifObj.pushMessageToParentDrop("[{\"id\":\""+rider_id+"\"}]",loc_lat,loc_lng,AppConfig.DROP_AT_HOME);
            }else{
                System.out.println("dropAtHome fail.");
            }
        }catch(UnirestException e){
            System.out.println("===="+e.getMessage());
        }
    }

    public void shortMessageServiceToParentPickUp(String rider_ids,String loc_lat,String loc_lng,String avail){
        System.out.println("shortMessageServiceToParent invokes..");
        try{
            String url_for_sms="http://139.59.24.227:9000/n/parent/pick/sms";
            String body_for_sms="rider_ids="+rider_ids+
                    "&loc_lat="+loc_lat+
                    "&loc_lng="+loc_lng+
                    "&avail="+avail;
            System.out.println("shortMessageServiceToParent body_for_sms="+body_for_sms);

            HttpResponse<String> response = Unirest.post(url_for_sms)
                    .header("content-type", "application/x-www-form-urlencoded")
                    .header("cache-control", "no-cache")
                    .body(body_for_sms)
                    .asString();
            System.out.println("shortMessageServiceToParent response Status"+response.getStatus());
            System.out.println("shortMessageServiceToParent response Body"+response.getBody());
            if(response.getStatus()==200){
                System.out.println("shortMessageServiceToParent success.");
            }else{
                System.out.println("shortMessageServiceToParent fail.");
            }
        }catch(UnirestException e){
            System.out.println("===="+e.getMessage());
        }

    }
    public void shortMessageServiceToParentDrop(String rider_ids,String loc_lat,String loc_lng,String avail){
        System.out.println("shortMessageServiceToParent invokes..");
        try{
            String url_for_sms="http://139.59.24.227:9000/n/parent/drop/sms";
            String body_for_sms="rider_ids="+rider_ids+
                    "&loc_lat="+loc_lat+
                    "&loc_lng="+loc_lng+
                    "&avail="+avail;
            System.out.println("shortMessageServiceToParent body_for_sms="+body_for_sms);

            HttpResponse<String> response = Unirest.post(url_for_sms)
                    .header("content-type", "application/x-www-form-urlencoded")
                    .header("cache-control", "no-cache")
                    .body(body_for_sms)
                    .asString();
            System.out.println("shortMessageServiceToParent response Status"+response.getStatus());
            System.out.println("shortMessageServiceToParent response Body"+response.getBody());
            if(response.getStatus()==200){
                System.out.println("shortMessageServiceToParent success.");
            }else{
                System.out.println("shortMessageServiceToParent fail.");
            }
        }catch(UnirestException e){
            System.out.println("===="+e.getMessage());
        }

    }
    public void pushMessageToParentPickUp(String rider_ids,String loc_lat,String loc_lng,String avail){
        System.out.println("pushMessageToParent invokes..");
        try{
            String url_for_push="http://139.59.24.227:9000/n/parent/pick/push";
            String body_for_push="rider_ids="+rider_ids+
                    "&loc_lat="+loc_lat+
                    "&loc_lng="+loc_lng+
                    "&avail="+avail;
            System.out.println("pushMessageToParent body_for_push="+body_for_push);

            HttpResponse<String> response = Unirest.post(url_for_push)
                    .header("content-type", "application/x-www-form-urlencoded")
                    .header("cache-control", "no-cache")
                    .body(body_for_push)
                    .asString();
            System.out.println("pushMessageToParent response Status: "+response.getStatus());
            System.out.println("pushMessageToParent response Body: "+response.getBody());
            if(response.getStatus()==200){
                System.out.println("pushMessageToParent success...");
            }else{
                System.out.println("pushMessageToParent fail...");
            }
        }catch(UnirestException e){
            System.out.println("===="+e.getMessage());
        }

    }
    public void pushMessageToParentDrop(String rider_ids,String loc_lat,String loc_lng,String avail){
        System.out.println("pushMessageToParent invokes..");
        try{
            String url_for_push="http://139.59.24.227:9000/n/parent/drop/push";
            String body_for_push="rider_ids="+rider_ids+
                    "&loc_lat="+loc_lat+
                    "&loc_lng="+loc_lng+
                    "&avail="+avail;
            System.out.println("pushMessageToParent body_for_push="+body_for_push);

            HttpResponse<String> response = Unirest.post(url_for_push)
                    .header("content-type", "application/x-www-form-urlencoded")
                    .header("cache-control", "no-cache")
                    .body(body_for_push)
                    .asString();
            System.out.println("pushMessageToParent response Status: "+response.getStatus());
            System.out.println("pushMessageToParent response Body: "+response.getBody());
            if(response.getStatus()==200){
                System.out.println("pushMessageToParent success...");
            }else{
                System.out.println("pushMessageToParent fail...");
            }
        }catch(UnirestException e){
            System.out.println("===="+e.getMessage());
        }

    }

    public static void main(String arg[]){
        RestApiInvoker obj=new RestApiInvoker();
        obj.pickUpFromHome("2","1","22.574761","88.43377","401","driver_email");
    }

}
