package webservice;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import mongo.CheckEvent;
import mongo.SendToMongo;
import utils.Driver;
import utils.TokenParser;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by satya on 1/2/18.
 */
public class GenerateToken {

    public String createToken(Driver driverObj){

        System.out.println("create token api called.");
        String access_token=null;
        try{
            String url_for_token="http://139.59.24.227:9000/token";
            String body_for_token="username="+driverObj.email+
                    "&password="+driverObj.password+
                    "&grant_type=password"+
                    "&client_id="+driverObj.client_id+
                    "&client_secret="+driverObj.client_secret;
            System.out.println("token call body="+body_for_token);

            HttpResponse<String> response = Unirest.post(url_for_token)
                    .header("content-type", "application/x-www-form-urlencoded")
                    .body(body_for_token)
                    .asString();
            System.out.println("getToken response Status"+response.getStatus());
            System.out.println("getToken response Body"+response.getBody());
            if(response.getStatus()==200){
                System.out.println("getToken success.");
                //update mongo driver access token with access_token created date.
                ObjectMapper mapper = new ObjectMapper();
                TokenParser tokenObj=new TokenParser();
                try{
                    tokenObj = mapper.readValue(response.getBody(), TokenParser.class);
                    SendToMongo monObj=new SendToMongo();
                    monObj.updateAccessToken(driverObj.email,tokenObj);
                    access_token=tokenObj.access_token;
                }catch(Exception e){
                    System.err.println("Exception at token response parsing..");
                }
            }else{
                System.out.println("getToken fail.");
            }
        }catch(UnirestException e){
            System.err.println("===="+e.getMessage());
        }
        System.out.println("Access_token : "+access_token);
        return access_token;
    }
    public String getRefreshToken(Driver driverObj,String refresh_token){

        System.out.println("create refresh_token api called.");
        String access_token=null;
        try{
            String url_for_refresh_token="http://139.59.24.227:9000/ref_token";
            String body_for_refresh_token="grant_type=refresh_token"+
                    "&client_id="+driverObj.client_id+
                    "&client_secret="+driverObj.client_secret+
                    "&refresh_token="+refresh_token;
            System.out.println("refresh_token call body="+body_for_refresh_token);

            HttpResponse<String> response = Unirest.post(url_for_refresh_token)
                    .header("content-type", "application/x-www-form-urlencoded")
                    .body(body_for_refresh_token)
                    .asString();
            System.out.println("getRefreshToken response Status"+response.getStatus());
            System.out.println("getRefreshToken response Body"+response.getBody());
            if(response.getStatus()==200){
                System.out.println("getToken success.");
                //update mongo driver access token with access_token created date.
                ObjectMapper mapper = new ObjectMapper();
                TokenParser tokenObj=new TokenParser();
                try{
                    tokenObj = mapper.readValue(response.getBody(), TokenParser.class);
                    SendToMongo monObj=new SendToMongo();
                    monObj.updateAccessToken(driverObj.email,tokenObj);
                    access_token=tokenObj.access_token;
                }catch(Exception e){
                    System.err.println("Exception at token response parsing..");
                }
            }else{
                System.out.println("getRefreshToken fail.");
            }
        }catch(UnirestException e){
            System.err.println("===="+e.getMessage());
        }

        System.out.println("Access Token Via Refresh Token : "+access_token);
        return access_token;
    }
    public Map<String,String> checkAccessTokenValidity(Driver dObj){
        Map<String,String> resMap=new HashMap<String,String>();
        boolean result=false;
        long created_hours=dObj.created_date/3600000;
        long current_hours=System.currentTimeMillis()/3600000;
        if((current_hours-created_hours)<6){
            result=true;
        }
        System.out.println("current_hours - created_hours :"+(current_hours-created_hours));
        resMap.put("result",String.valueOf(result));
        if(result){
            resMap.put("access_token",dObj.access_token);
        }else{
            resMap.put("refresh_token",dObj.refresh_token);
        }
        return resMap;
    }
    public String getToken(){
        String access_token=null;
        CheckEvent objE=new CheckEvent();
        Driver driverObj=objE.getDriver("0007419047");
        System.out.println("Driver access_token : "+driverObj.access_token);
        if(driverObj.access_token==null || driverObj.access_token.equals("") ){
            System.out.println("Access Token Not Found...");
            access_token=createToken(driverObj);
        }else{
            System.out.println("Access Token Found...");
            System.out.println("Checking access token validity...");
            Map<String,String> resMap=checkAccessTokenValidity(driverObj);
            if(resMap.get("result").equals("true")){
                access_token=resMap.get("access_token");
            }else{
                access_token=getRefreshToken(driverObj,resMap.get("refresh_token"));
            }
        }
        return access_token;
    }
    public static void main(String []args){

        GenerateToken obj = new GenerateToken();
        System.out.println("Result : "+obj.getToken());
    }
}
