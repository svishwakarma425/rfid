package pgdb;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Created by satya on 10/1/18.
 */
public class DB_Connection {
    public static Connection getDbConnection(){
        System.out.println("-------- PostgreSQL JDBC Connection Testing ------------");

        try {

            Class.forName("org.postgresql.Driver");

        } catch (ClassNotFoundException e) {

            System.out.println("Where is your PostgreSQL JDBC Driver? "
                    + "Include in your library path!");
            e.printStackTrace();
            return null;

        }

        System.out.println("PostgreSQL JDBC Driver Registered!");

        Connection connection = null;

        try {
            String HOST="jdbc:postgresql://hexa.crhsa2hfbow2.ap-southeast-1.rds.amazonaws.com:5432/dbhex";//System.getenv("PG_HOST");//jdbc:postgresql://hexa.crhsa2hfbow2.ap-southeast-1.rds.amazonaws.com:5432/dbhex
            String USERNAME="dbhexdev";System.getenv("PG_USERNAME");//dbhexdev
            String PASSWORD="Patdbhexdev";//System.getenv("PG_PASSWORD");//Patdbhexdev
            //System.out.println("HOST="+HOST+"\n USERNAME="+USERNAME+"\n PASSWORD="+PASSWORD);

            connection = DriverManager.getConnection(
                    HOST, USERNAME,
                    PASSWORD);

        } catch (SQLException e) {

            System.out.println("Connection Failed! Check output console");
            e.printStackTrace();
            return null;

        }

        if (connection != null) {
            System.out.println("You made it, take control your database now!");
        } else {
            System.out.println("Failed to make connection!");
        }

        return connection;
    }
}
