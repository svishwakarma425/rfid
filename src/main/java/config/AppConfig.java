package config;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by asitm9 on 24/5/17.
 */
public class AppConfig {

    public static final String READY_TO_PICKUP_FROM_HOME="0";
    public static final String PICKUP_FROM_HOME="1";
    public static final String DROP_AT_SCHOOL="2";
    public static final String PICKUP_FROM_SCHOOL="4";
    public static final String DROP_AT_HOME="5";
    public static final String USER_ACTIVITY_LOG="user_activity_log";
    public static final String MONGO_CLIENT_URL=getMongoClientUrl();
    public static final String MONGO_DB="api";//getMongoDB();
    public static final String TOPIC_NAME=getTopic();
    public static final boolean PRODUCTION=false;
    public static final boolean TEST=true;
    public static final String LOCATION_UPDATE_URL=getLocationUpdateUrl();
    public static final String RFID_EVENT="rfid_event";
    public static final String RFID_DRIVER="rfid_driver";


    public static final String DRIVER="driver";
    public static final String STUDENT="student";


    
    //public static Logger slf4jLogger =  LoggerFactory.getLogger(AppConfig.class);

    public static String getLocationUpdateUrl(){
        String url=null;
        if(PRODUCTION){
            url="http://4alzlx.hexarides.com//PVmjVgB0Xh";
        }else{
            url="http://4alzlx.hexarides.com/T2kKcZenR7";
        }
        return url;
    }

    public static String getMongoClientUrl() {

        String mongo_url=null;
        try {

            String MONGO_HOST_PRIMARY="g91.hexarides.com";//System.getenv("MONGO_HOST_PRIMARY");
            String MONGO_HOST_SECONDARY="g92.hexarides.com";//System.getenv("MONGO_HOST_SECONDARY");
            String MONGO_PORT="27017";//System.getenv("MONGO_PORT");
            String MONGO_USERNAME="rpr";//System.getenv("MONGO_USERNAME");
            String MONGO_PASSWORD="yWhJ630nIHVDkhPpInYZS9AK9PBnxVT7";//System.getenv("MONGO_PASSWORD");
            String NO_SQL="mongodb";//System.getenv("NO_SQL");
            String AUTH_SOURCE="admin";//System.getenv("AUTH_SOURCE");
            String REPLICA_SET="rp0";//System.getenv("REPLICA_SET");
            mongo_url=NO_SQL+"://"+MONGO_USERNAME+":"+MONGO_PASSWORD+"@"+MONGO_HOST_PRIMARY+":"+MONGO_PORT+","+MONGO_HOST_SECONDARY+":"+MONGO_PORT+"/?authSource="+AUTH_SOURCE+"&replicaSet="+REPLICA_SET;
            //mongo_url="mongodb://localhost:27017";

            System.out.println("AAAA mongo_url to access= "+mongo_url);
            //slf4jLogger.debug("mongo_url to access= "+mongo_url);

            return mongo_url;
        }catch(Exception e) {
            System.err.println("Exception at getMongoClientUrl="+e.getMessage());
            //slf4jLogger.debug("Exception at getMongoClientUrl="+e.getMessage());
            return "";
        }

    }
    public static String getMongoDB() {

        try {
            String MONGO_DB=null;
            if(AppConfig.PRODUCTION){
                MONGO_DB="prod";//System.getenv("MONGO_DB");
            }else if(AppConfig.TEST){
                MONGO_DB="api";//System.getenv("TEST_MONGO_DB");
            }
            return MONGO_DB;
        }catch(Exception e) {
            System.err.println("Exception at getMongoDB="+e.getMessage());
            //slf4jLogger.debug("Exception at getMongoDB="+e.getMessage());
            return "";
        }

    }
    public static String getTopic() {

        try {
            String TOPIC_NAME=null;
            if(AppConfig.PRODUCTION){
                TOPIC_NAME=System.getenv("TOPIC_NAME");
            }else if(AppConfig.TEST){
                TOPIC_NAME="rfid";//System.getenv("TOPIC_NAME");
            }
            return TOPIC_NAME;
        }catch(Exception e) {
            System.err.println("Exception at getTopic="+e.getMessage());
            //slf4jLogger.debug("Exception at getTopic="+e.getMessage());
            return "";
        }

    }

}
