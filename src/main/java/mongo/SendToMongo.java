package mongo;

import com.mongodb.MongoException;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.result.UpdateResult;
import config.AppConfig;
import org.bson.Document;
import utils.TokenParser;

import static com.mongodb.client.model.Filters.*;

import static com.mongodb.client.model.Updates.combine;
import static com.mongodb.client.model.Updates.set;

/**
 * Created by satya on 31/1/18.
 */
public class SendToMongo {
    public boolean  save(String rfid_data){
        boolean result=false;
        try {

            MongoCollection<Document> rfid_event_collection = MongoConnector.getConnection(AppConfig.RFID_EVENT);
            Document doc = Document.parse(rfid_data);
            doc.put("created_date",System.currentTimeMillis());
            rfid_event_collection.insertOne(doc);
            result=true;
            System.out.println("rfid data inserted!!!");

        }  catch (MongoException e) {
            System.err.println("Exception at : "+e.getMessage());
        }
        return result;

    }

    public void updateAccessToken(String email, TokenParser tokenObj){
        try {

            MongoCollection<Document> rfid_driver_collection = MongoConnector.getConnection(AppConfig.RFID_DRIVER);
            UpdateResult updateResult = rfid_driver_collection.updateOne(eq("email", email),
                                        combine(set("access_token", tokenObj.access_token),
                                                set("refresh_token",tokenObj.refresh_token),
                                                set("created_date",System.currentTimeMillis())));
            System.out.println("Token Modified Count : "+updateResult.getModifiedCount());
        }  catch (MongoException e) {
            System.err.println("Exception at : "+e.getMessage());
        }
    }


}
