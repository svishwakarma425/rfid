package mongo;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.MongoSocketOpenException;
import com.mongodb.MongoTimeoutException;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import config.AppConfig;
import org.bson.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * Created by asitm9 on 24/5/17.
 */
public class MongoConnector {
    public static MongoDatabase database=null;
    public static MongoClient mongoClient=null;
    //public static Logger slf4jLogger = null;//LoggerFactory.getLogger(MongoConnector.class);

    public static MongoCollection<Document> getConnection(String collectionName){


        MongoCollection<Document> collection=null;
        MongoDatabase database=null;
        try {
            //slf4jLogger = LoggerFactory.getLogger(MongoConnector.class);
            MongoClientURI connectionString = new MongoClientURI(AppConfig.MONGO_CLIENT_URL);
            //@SuppressWarnings("resource")
            //MongoClient mongoClient = new MongoClient(connectionString);
            if(mongoClient==null){
                mongoClient = new MongoClient(connectionString);
            }
            // Now connect to your databases
            if(database==null){
                database = mongoClient.getDatabase(AppConfig.MONGO_DB);//getDB( "users" );
            }

            collection=database.getCollection(collectionName);
            System.out.println("Connect to db: "+AppConfig.MONGO_DB+" and collection: "+collectionName+" successfully");
            //slf4jLogger.debug("Connect to db: "+AppConfig.MONGO_DB+" and collection: "+collectionName+" successfully");
        }catch(MongoSocketOpenException e){
            System.err.println( "MongoSocketOpenException ="+e.getClass().getName() + ": " + e.getMessage() );
            //slf4jLogger.debug("MongoSocketOpenException ="+e.getClass().getName() + ": " + e.getMessage());
        }catch(MongoTimeoutException e){
            System.err.println( "MongoTimeoutException ="+e.getClass().getName() + ": " + e.getMessage() );
            //slf4jLogger.debug("MongoTimeoutException ="+e.getClass().getName() + ": " + e.getMessage());
        }catch(Exception e){
            System.err.println( "Exception at getConnection="+e.getClass().getName() + ": " + e.getMessage() );
            //slf4jLogger.debug("Exception at getConnection="+e.getClass().getName() + ": " + e.getMessage() );
        }
        return collection;
    }

}
