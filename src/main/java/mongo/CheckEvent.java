package mongo;

import com.mongodb.MongoException;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.model.Projections;
import config.AppConfig;
import org.bson.Document;
import utils.Driver;

import static com.mongodb.client.model.Filters.*;

/**
 * Created by satya on 31/1/18.
 */
public class CheckEvent {

    public String getEventFor(String uid){
        String event_for=null;
        try{
            MongoCollection<Document> rfid_driver_collection = MongoConnector.getConnection(AppConfig.RFID_DRIVER);
            FindIterable<Document> cursor = rfid_driver_collection.find(eq("uid",uid)).projection(Projections.exclude("_id"));

            int cursor_size=0;
            //Driver driObj=new Driver();
            for (Document cur : cursor) {
                cursor_size=cursor_size+1;
                //driObj.uid=cur.getString("uid");
            }
            if(cursor_size==0){
                event_for="student";
            }else if(cursor_size>0){
                event_for="driver";
            }

        }catch(MongoException e){
          System.err.println("Exception at getEventFor : "+e.getMessage());
        }
        System.out.println("event_for="+event_for);
        return event_for;

    }
    public Driver getDriver(String rfid_number){
        Driver driObj=new Driver();
        try{
            MongoCollection<Document> rfid_driver_collection = MongoConnector.getConnection(AppConfig.RFID_DRIVER);
            FindIterable<Document> cursor = rfid_driver_collection.find(eq("rfid_number",rfid_number)).projection(Projections.exclude("_id"));

            int cursor_size=0;

            for (Document cur : cursor) {
                cursor_size=cursor_size+1;
                driObj.rfid_number=cur.getString("rfid_number");
                driObj.email=cur.getString("email");
                driObj.password=cur.getString("password");
                driObj.client_id=cur.getString("client_id");
                driObj.client_secret=cur.getString("client_secret");
                driObj.access_token=cur.getString("access_token");
                driObj.refresh_token=cur.getString("refresh_token");
                driObj.created_date=cur.getLong("created_date");
            }
            System.out.println("cursor_size : "+cursor_size);

        }catch(MongoException e){
            System.err.println("Exception at getEventFor : "+e.getMessage());
        }
        System.out.println("driObj Email : "+driObj.email);
        return driObj;

    }
    public int getDriverTouchCount(String rfid_number){
        try{
            MongoCollection<Document> rfid_driver_collection = MongoConnector.getConnection(AppConfig.RFID_EVENT);
            FindIterable<Document> cursor =null;
               long t=rfid_driver_collection.count(eq("info.cmdval",rfid_number));
                            //find(eq("info.cmdval",rfid_number)).;
            Document doc=rfid_driver_collection.find().first();
            System.out.println("cursor_size :  "+doc.get("info"));
            //Document doc1=doc.toJson();

        }catch(MongoException e){
            System.err.println("Exception at getEventFor : "+e.getMessage());
        }
        System.out.println("driObj Email : ");
        return 1;
    }

    public static void main(String []args){
        CheckEvent obj = new CheckEvent();
        obj.getDriverTouchCount("12345677");
    }
}
