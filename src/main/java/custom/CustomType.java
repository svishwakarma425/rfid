package custom;

/**
 * Created by asitm9 on 21/4/17.
 */
import com.lambdaworks.redis.protocol.LettuceCharsets;
import com.lambdaworks.redis.protocol.ProtocolKeyword;

public enum CustomType implements ProtocolKeyword {
    ETA;

    public final byte[] bytes;

    private CustomType() {
        this.bytes = this.name().getBytes(LettuceCharsets.ASCII);
    }

    public byte[] getBytes() {
        return this.bytes;
    }
}
