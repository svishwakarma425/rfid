package utils;

/**
 * Created by satya on 1/2/18.
 */
public class TokenParser {
    public String access_token;
    public String token_type;
    public String expires_in;
    public String refresh_token;
    public String scope;
}
