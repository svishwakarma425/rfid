package utils;

/**
 * Created by satya on 11/1/18.
 */
public class JsonTransmitProtocolSchema {
    public String uid;
    public Info info;
    public  class Info {
        public long dt;
        public String txn;
        public Integer msgkey;
        public Integer msgid;
        public String cmdkey;
        public String cmdval;
    }
    public Gps gps;
    public  class Gps {
        public String fix;
        public String[] loc;
        public Integer speed;
        public Integer sat;
        public Integer alt;
        public Integer dir;
        public Integer odo;
    }
    public Io io;
    public  class Io {
        public Integer box;
        public Integer ign;
        public Integer gpi;
        public Integer analog;
    }
    public Pwr pwr;
    public  class Pwr {
        public Integer main;
        public Integer batt;
        public Integer volt;
    }
    public Dbg dbg;
    public  class Dbg {
        public Integer[] status;
        public String[] ver;
        public String lib;
    }
}
