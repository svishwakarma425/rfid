package utils;

/**
 * Created by satya on 11/1/18.
 */
public class Driver {
    public String rfid_number;
    public String user_id;
    public String email;
    public String password;
    public String client_id;
    public String client_secret;
    public String access_token;
    public String refresh_token;
    public long created_date;
}
