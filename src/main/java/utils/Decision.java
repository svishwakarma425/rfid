package utils;

import config.AppConfig;

import java.util.Map;

/**
 * Created by satya on 11/1/18.
 */
public class Decision {

    public static String decideStudentLocation(Map<String,String> stdStateMap){
        //long range=1000;
        //long distance_from_home=Long.parseLong(stdLocMap.get("distance_from_home"));
        //long distance_from_school=Long.parseLong(stdLocMap.get("distance_from_school"));
        String decision=null;
        String avail=stdStateMap.get("avail");
        if(avail.equals(AppConfig.READY_TO_PICKUP_FROM_HOME) && true){//distance_from_home<range){
            decision= AppConfig.PICKUP_FROM_HOME;
        }else if(avail.equals(AppConfig.PICKUP_FROM_HOME) && true){//distance_from_school<range){
            decision=AppConfig.DROP_AT_SCHOOL;
        }else if(avail.equals(AppConfig.DROP_AT_SCHOOL) && true){//distance_from_school<range){
            decision=AppConfig.PICKUP_FROM_SCHOOL;
        }else if(avail.equals(AppConfig.PICKUP_FROM_SCHOOL) && true){//distance_from_home<range){
            decision=AppConfig.DROP_AT_HOME;
        }else{
            decision="Invalid state: "+avail;
        }
        System.out.println("decision="+decision);
        return decision;
    }
}
