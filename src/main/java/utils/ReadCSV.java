package utils;

import com.opencsv.CSVReader;

import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by satya on 11/1/18.
 */
public class ReadCSV {
    public static Driver getDriver(String uid){
        Driver driverObj=new Driver();
        try {
            //String CSV_FILE_PATH="/home/satya/Application/driver.csv";//System.getenv("CSV_FILE_PATH");
            String CSV_FILE_PATH="/root/Karma/rfid/maven_service/driver.csv";//System.getenv("CSV_FILE_PATH");
            System.out.println("CSV_FILE_PATH="+CSV_FILE_PATH);
            Reader reader = Files.newBufferedReader(Paths.get(CSV_FILE_PATH));
                CSVReader csvReader = new CSVReader(reader);

            // Reading Records One by One in a String array
            String[] nextRecord;

            int first=0;
            while ((nextRecord = csvReader.readNext()) != null) {
                if(first!=0){
                    //Driver obj=new Driver();
                    if(uid.equals(nextRecord[0])) {
                        /**
                        System.out.println("uid : " + nextRecord[0]);
                        System.out.println("user_id : " + nextRecord[1]);
                        System.out.println("driver_email : " + nextRecord[2]);
                        System.out.println("password : " + nextRecord[3]);
                        System.out.println("client_id : " + nextRecord[4]);
                        System.out.println("client_secret : " + nextRecord[5]);
                         **/

                        driverObj.rfid_number = nextRecord[0];
                        driverObj.user_id = nextRecord[1];
                        driverObj.email = nextRecord[2];
                        driverObj.password = nextRecord[3];
                        driverObj.client_id = nextRecord[4];
                        driverObj.client_secret = nextRecord[5];
                    }
                    //driverList.add(obj);
                }
                first=first+1;
            }
            System.out.println("driver email====>"+driverObj.email);
        }catch (IOException e){
            System.out.println("IOException at readCSV:"+e.getMessage());
        }
        return driverObj;
    }
}
