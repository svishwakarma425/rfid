package utils;

import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Created by satya on 11/1/18.
 */
public class Test {

    public static void main(String arg[]){
        String value="{\"uid\":\"1739010014027\",\"info\":{\"dt\":1072915226,\"txn\":\"W\",\"msgkey\":0,\"msgid\":1,\"cmdkey\":\"RFID\",\"cmdval\":\"1712354\"},\"gps\":{\"fix\":\"V\",\"loc\":[0.0000000,0.0000000],\"speed\":0,\"sat\":0,\"alt\":0,\"dir\":0,\"odo\":693},\"io\":{\"box\":1,\"ign\":0,\"gpi\":0,\"analog\":29},\"pwr\":{\"main\":1,\"batt\":0,\"volt\":4778},\"dbg\":{\"status\":[1,1,30,13,3,0],\"ver\":[\"3.6JCRF\",\"1.0\"],\"lib\":\"6.10\"}}";

        ObjectMapper mapper = new ObjectMapper();
        JsonTransmitProtocolSchema jsonReqObj=new JsonTransmitProtocolSchema();
        try {
            jsonReqObj = mapper.readValue(value, JsonTransmitProtocolSchema.class);
            System.out.println("===="+jsonReqObj.info.dt);
        }catch(Exception e){
            System.err.println("Ex "+e.getMessage());
        }


        }
}
